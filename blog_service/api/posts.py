from fastapi import APIRouter, Depends
from dependencies import get_client, get_db
from datamodels import PostsApiInput, PostsApiOutput, Post
import asyncio
import config

router = APIRouter()


@router.post("/posts", response_model=PostsApiOutput)
async def sentences(
    api_input: PostsApiInput, http_client=Depends(get_client), db=Depends(get_db)
) -> PostsApiOutput:
    # Save the post in the DB with no flag
    db[api_input.title] = Post(api_input.paragraphs, None)

    # break paragraphs into sentences
    _sentences = [paragraph.split(".") for paragraph in api_input.paragraphs]
    sentences = [sentence for sublist in _sentences for sentence in sublist]

    # Create a request coroutine for every sentence in the post
    aws = [
        asyncio.create_task(
            http_client.post(
                f"{config.profanity_service_url}/sentences",
                json={"fragment": fragment},
            )
        )
        for fragment in sentences
    ]

    # We need to keep 2 things in mind
    # 1. If just one sentence is profane, we can mark the post as profane
    # 2. If some of the requests fail, we cannot be certain the post is not profane
    #
    # So we will concurrently make requests, and check the results. If the results are positive
    # then we will stop querying for more, even if other requests have failed.
    # If by the end of the requests we have not seen a profane flag but have encountered errors,
    # we will not set any flags (maybe the sentences that we missed are profane? We don't know).
    # If all the requests are completed and none of them are marked as profane, then we
    # can safely set the flag to false.
    encountered_errors = False
    has_profanity = False

    # Make every request concurrently and process them as they arrive
    for future in asyncio.as_completed(aws):
        try:
            response = await future
            result = response.json()
            if "hasFoulLanguage" in result:
                # The service has returned
                has_profanity = result["hasFoulLanguage"]
                # If there's even 1 profane sentece in the blogpost,
                # we can safely mark it as such. So we will break the look
                # and cancel the remaining requests.
                if has_profanity:
                    break
            else:
                # Returned result not in expected format.
                encountered_errors = True
        except:
            # Couldn't reach the service
            encountered_errors = True

    if has_profanity:
        # We have seen at least 1 profane sentece, mark as such
        db[api_input.title].has_foul_language = True
    elif encountered_errors:
        # We have encountered errors. The sentences that were not processed might have
        # been profane. Put no flag in the db.
        db[api_input.title].has_foul_language = None
    else:
        # No errors and no profane sentences. Set the flag to false.
        db[api_input.title].has_foul_language = False
    return PostsApiOutput(db[api_input.title].has_foul_language)

from fastapi import APIRouter
import api.posts

api_router = APIRouter()

api_router.include_router(api.posts.router, tags=["posts"])

from httpx import AsyncClient
from datamodels import Post
from typing import Dict


_db: Dict[str, Post] = {}


async def get_client() -> AsyncClient:
    async with AsyncClient() as session:
        yield session


def get_db() -> Dict[str, Post]:
    return _db

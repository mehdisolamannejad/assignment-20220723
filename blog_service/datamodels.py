from dataclasses import dataclass
from typing import List, Optional


@dataclass
class PostsApiInput:
    title: str
    paragraphs: List[str]


@dataclass
class PostsApiOutput:
    is_profane: Optional[bool]


@dataclass
class Post:
    paragraphs: List[str]
    has_foul_language: Optional[bool]

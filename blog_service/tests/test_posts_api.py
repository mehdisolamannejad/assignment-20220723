from urllib import response
from fastapi.testclient import TestClient
import httpx
import json

def profanity_service_mock(request: httpx.Request):
    body = json.loads(request.read())
    if body["fragment"] == "profane":
        return httpx.Response(status_code=200, json={"hasFoulLanguage": True})
    elif body["fragment"] == "fail":
        return httpx.Response(status_code=500)
    else:
        return httpx.Response(status_code=200, json={"hasFoulLanguage": False})

def test_sentences_api(client: TestClient, httpx_mock):
    httpx_mock.add_callback(profanity_service_mock)
    # Test blog post with no profanity
    resp = client.post("/posts", json={"title": "a", "paragraphs": ["a", "b", "c"]})
    assert resp.status_code == 200
    assert resp.json()["is_profane"] == False
    # Test blog post with 1 profanity
    resp = client.post("/posts", json={"title": "a", "paragraphs": ["a", "b", "profane"]})
    assert resp.status_code == 200
    print(resp.json())
    assert resp.json()["is_profane"] == True
    # Test blog post with 1 profanity and 1 failure
    resp = client.post("/posts", json={"title": "a", "paragraphs": ["a", "fail", "profane"]})
    assert resp.status_code == 200
    assert resp.json()["is_profane"] == True
    # Test blog post with 1 failure
    resp = client.post("/posts", json={"title": "a", "paragraphs": ["a", "fail", "c"]})
    assert resp.status_code == 200
    assert resp.json()["is_profane"] == None
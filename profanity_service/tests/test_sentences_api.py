from fastapi.testclient import TestClient

def test_sentences_api(client: TestClient):
    # Test profane sentence
    resp = client.post("/sentences", json={"fragment": "hell"})
    assert resp.status_code == 200
    assert resp.json()["hasFoulLanguage"] == True
    # Test non-profane sentence
    resp = client.post("/sentences", json={"fragment": "hello"})
    assert resp.status_code == 200
    assert resp.json()["hasFoulLanguage"] == False
    # Test bad format
    # Test profane sentence
    resp = client.post("/sentences", json={"sentence": "hell"})
    assert resp.status_code == 422
from dataclasses import dataclass


@dataclass
class SentencesApiInput:
    fragment: str


@dataclass
class SentencesApiOutput:
    hasFoulLanguage: bool

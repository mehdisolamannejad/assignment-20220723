from fastapi import APIRouter
import api.sentences

api_router = APIRouter()

api_router.include_router(api.sentences.router, tags=["sentences"])

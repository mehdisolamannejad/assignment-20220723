from fastapi import APIRouter
from datamodels import SentencesApiInput, SentencesApiOutput
from ml.profanity_model import classify_profanity

router = APIRouter()


@router.post("/sentences", response_model=SentencesApiOutput)
async def sentences(api_input: SentencesApiInput) -> SentencesApiOutput:
    classification_result = classify_profanity(api_input.fragment)
    return SentencesApiOutput(classification_result)

profane_words = ["damn", "hell", "bastard", "crap", "prick", "shit"]


def classify_profanity(sentence: str) -> bool:
    words = sentence.split(" ")
    for word in words:
        if word in profane_words:
            return True

    return False

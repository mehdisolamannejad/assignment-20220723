fastapi==0.79.0
httpx==0.23.0
uvicorn==0.18.2
pytest==7.1.2
pytest-httpx==0.21.0
requests==2.28.1
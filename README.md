This is a repo for an interview assignment.  
The interesting file is `blog_service/api/posts.py`.

## How to run the services?
This assignment includes 2 services. After installing all the requirements from `requirements.txt`,
you should be able to run both of them.
```
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

##### Profanity service
Run `profanity_service/main.py`. The application will listen on port 5001.
````
python profanity_service/main.py
````

##### Blog service
Run `blog_service/main.py`. The application will listen on port 5000.
````
python blog_service/main.py
````

## How to run tests?
Pytest has been used to write tests for both the services. 

##### Profanity service
```
pytest profanity_service
```
##### Blog service
```
pytest blog_service
```

## Used libraries
FastAPI - for creating the APIs  
Uvicorn - as a web server  
HTTPX - as a http client  
PyTest - as a testing framework (needs pytest-httpx)  

Note: requests is only included because fastapi.TestClient needs it to properly operate.